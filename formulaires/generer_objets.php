<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function formulaires_generer_objets_charger_dist($redirect = '') {
	$valeurs = [
		'rubrique'   => 0,
		'article'    => 0,
		'mot'        => 0,
		'sup_objets' => null,
		'del_num'    => null
	];

	return $valeurs;
}

function formulaires_generer_objets_verifier_dist($redirect = '') {
	$erreurs = [];
	return $erreurs;
}

function formulaires_generer_objets_traiter_dist($redirect = '') {

	$retour = [];
	$retour['message_ok'] = '';
	$sup_objets = _request('sup_objets');

	if ($sup_objets) {
		foreach (['rubrique', 'article', 'mot'] as $objet) {
			$table = table_objet_sql($objet);
			sql_delete($table, ['texte="test_delnum"' ]);
		}
		$retour['message_ok'] .= 'Suppression des objets de test OK <br><br>';
	}

	foreach (['rubrique', 'article', 'mot'] as $objet) {
		$nb =  (int) _request($objet);
		if ($nb) {
			$retour['message_ok'] .= "Ajout de $nb $objet(s) <br>";
			$table = table_objet_sql($objet);
			$nb++;
			for ($i = 1; $i < $nb; $i++) {
				sql_insertq($table, [
					'titre'     => $i . '. ma_rubrique '. $i,
					'texte'     => 'test_delnum',
				]);
			}
		}
	}

	if (_request('del_num')) {
		include_spip('delnum_administrations');

		spip_timer('delnum');
		$nbRub = sql_countsel('spip_rubriques', ['texte="test_delnum"']);
		$nbArt = sql_countsel('spip_articles', ['texte="test_delnum"']);
		$nbMot = sql_countsel('spip_mots', ['texte="test_delnum"']);
		delnum_num_titre_en_rang('rubriques');
		delnum_num_titre_en_rang('articles');
		delnum_num_titre_en_rang('mots');
		$duree = spip_timer('delnum');
		$retour['message_ok'] .= '<br>';
		$retour['message_ok'] .= 'Temps d\'execution du script de transformation = '. $duree;
		$retour['message_ok'] .= '<br>';
		$retour['message_ok'] .= $nbRub . ' rubriques';
		$retour['message_ok'] .= '<br>';
		$retour['message_ok'] .= $nbArt . ' articles';
		$retour['message_ok'] .= '<br>';
		$retour['message_ok'] .= $nbMot . ' mots';
	}

	if ($redirect) {
		$retour['redirect'] = $redirect;
	}

	return $retour;
}
