<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}




function delnum_declarer_tables_principales($tables) {
	$tables['spip_rubriques']['field']['rang'] = 'SMALLINT NOT NULL';
	$tables['spip_articles']['field']['rang']  = 'SMALLINT NOT NULL';
	$tables['spip_mots']['field']['rang']      = 'SMALLINT NOT NULL';
	return $tables;
}
