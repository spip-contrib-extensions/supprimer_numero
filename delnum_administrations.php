<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}



/**
 * Fonction d'installation et de mise à jour du plugin Gformation.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
**/
function delnum_upgrade($nom_meta_base_version, $version_cible) {
	$maj = [];

	$maj['create'] = [ ['maj_tables', ['spip_rubriques']]];

	$maj['1.0.2'] = [
		['maj_tables', ['spip_rubriques']],
	];
	$maj['1.0.3'] = [
		['delnum_num_titre_en_rang', 'rubriques'],
	];
	$maj['1.0.4'] = [
		['maj_tables', ['spip_articles']],
		['delnum_num_titre_en_rang', 'articles'],
	];
	$maj['1.0.5'] = [
		['maj_tables', ['spip_mots']],
		['delnum_num_titre_en_rang', 'mots'],
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

function delnum_num_titre_en_rang(string $objet):void {
	$table = table_objet_sql($objet);

	$trouver_table = charger_fonction('trouver_table', 'base');
	$desc          = $trouver_table($table);
	$tChamps       = $desc['field'];
	if (!array_key_exists('rang', $tChamps)) {
		spip_log('Il manque le champ rang dans : '. $table, 'delnum_error');
	}

	$id_table_objet = id_table_objet($table);

	$w = [
		"titre REGEXP '^[0-9]+[.][[:space:]]'",
		// "rang=0"
	];
	$s = [$id_table_objet, 'titre'];

	include_spip('inc/filtres');
	$res = sql_allfetsel($s, $table, $w);
	$t = [];
	if (!empty($res)) {
		foreach ($res as $d) {
			$titre = $d['titre'];
			$rang  = (int) recuperer_numero($titre);
			$titre = supprimer_numero($titre);
			if ($rang) {
				sql_updateq($table, ['titre' => $titre, 'rang' => $rang], "$id_table_objet =".intval($d[$id_table_objet]));
				$t[] = $d[$id_table_objet];
			}
		}
		spip_log('Modification du titre pour '. count($t) . ' ' . $objet, 'delnum_'.$objet);
		spip_log($t, 'delnum_'.$objet);
	}
}
