# Supprimer numero

![delnum](delnum.svg)


Migrer les numéros de titres des tables dobjets dans un champ rang

## tests
il y a un formulaire de test : `?page=generer_objets` qui permet :
* creer des rubriques / articles / mots avec un num objet
* supprimer ces objets
* lancer le script de transformation des num objets en rang avec un retour sur le temps de traitement

exemple de retour :
```text
Temps d'execution du script de transformation = 37 637.064 ms
pour :
999 rubriques
9999 articles
999 mots
```
